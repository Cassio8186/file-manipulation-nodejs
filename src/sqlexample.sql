# Neste exemplo utilizaremos a sintax do mariaDB.
# https://mariadb.com/kb/en/library/sql-statements/

# Primeiro criaremos o banco de dados que será utilizado no exemplo

# banco chamado newDatabase com tipo de codificação binária utf-8 e será criado somente se não existir outro banco com o mesmo nome.
CREATE DATABASE if NOT exists newDatabase DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

# informamos que para os próximos comandos utilizaremos a newdatabase criada previamente.
use newDatabase;

# Primeiro criaremos uma tabela que é onde dados podem ser armazenados.
# Nossa tabela terá o id que vai ser auto incrementado (a cada dado inserido o id será incrementado)
# Atributo nome que será varchar e legal que receberá um bit. 1 para aluno legal e 0 para aluno chato
CREATE TABLE if NOT EXISTS `student` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`nome` VARCHAR(50) NOT NULL COLLATE 'utf8_unicode_ci',
	`legal` BIT(1) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `nome` (`nome`)
) COLLATE = 'utf8_unicode_ci' ENGINE = InnoDB;

# Após a execução dos comandos nossa estrutura deve estar montada.
# Agora inseriremos alguns alunos a essa tabela.
INSERT INTO	student (nome, legal) VALUES ('Cassio', 1);
INSERT INTO	student (nome, legal) VALUES ('Paulo', 1);
INSERT INTO	student (nome, legal) VALUES ('Alexandre', 1);
INSERT INTO	student (nome, legal) VALUES ('Anderson', 1);
INSERT INTO	student (nome, legal) VALUES ('Fabricio', 1);

# Após a execução dos comandos, na nossa tabela teremos
# "id"	"nome"		"legal"
# "1"	"Cassio"	"1"
# "2"	"Paulo"		"1"
# "3"	"Alexandre"	"1"
# "4"	"Anderson"	"1"
# "5"	"Fabricio"	"1"

# Agora removeremos o aluno Fabricio pois ele não faz parte do nosso grupo.
# Neste comando nós informamos que queremos deletar dados de uma tabela e informamos
# a condição para achar o dado.
DELETE FROM	student WHERE	nome = 'fabricio';

# Falta somente alterar um dado que é do aluno Anderson
# Por sabermos que o anderson não é legal devemos alterar esta informação.

UPDATE	student SET	legal = 0 WHERE	nome = 'Anderson';

# Para visualizar os dados criados, podemos utilizar o comando
SELECT	* FROM	student;

# onde ele retornará todos os campos da tabela student
# "id" "nome"	"legal"
# "1"	"Cassio"	"1"
# "2"	"Paulo"		"1"
# "3"	"Alexandre"	"1"
# "4"	"Anderson"	"0"