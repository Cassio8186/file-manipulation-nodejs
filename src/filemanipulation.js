// Este exemplo utiliza conceitos do ES6, como arrow function.

// Utilizaremos o módulo do node js file system que será responsável por manipular os arquivos neste exemplo.
const filesystem = require("fs");

// Módulo que manipulará os caminhos onde os arquivos serão salvos.
const path = require("path");

// Essa utilizaremos essa variável para informar ao computador que o tipo de codificação binária utilizada será utf-8
const utf8 = { encoding: "utf8" };

// Cria pasta (se não existe) que será utilizada para manipulação de arquivos deste programa
if (!filesystem.existsSync(path.join(__dirname, "/arquivos/"))) {
	const arquivosDir = path.join(__dirname, "/arquivos/");
	filesystem.mkdirSync(arquivosDir);
}
/**
 * @function Criará um novo caminho na pasta atual com o filename escolhido.
 * Exemplo de utilização: newPath("novo-arquivo.txt")
 * Função retornará o caminho:
 * C:\Users\cassio\Desktop\Git\Cassio\file-manipulation-nodejs\teste.txt
 */

const newPath = filename => {
	return path.join(__dirname, "/arquivos", filename);
};

/**
 * Cria um arquivo chamado newfile, em utf-8 cujo conteúdo vai ser a frase: Conteúdo presente no arquivo
 */
filesystem.writeFileSync(
	newPath("newfile.txt"),
	"Conteúdo presente no arquivo",
	utf8
);

// uma das formas de editar um arquivo é sobrescrevendo
filesystem.writeFileSync(
	newPath("newfile.txt"),
	"Arquivo foi sobrescrito",
	utf8
);

// Há também maneiras que permitem que seja adicionado conteúdo ao arquivo
filesystem.appendFileSync(
	newPath("newfile.txt"),
	"\nadicionando linha ao arquivo já criado",
	utf8
);
/**
 * Cria um outro arquivo que será modificado
 */
filesystem.writeFileSync(
	newPath("seraremovido.txt"),
	"Este arquivo será modificado",
	utf8
);

// Remove um arquivo chamado seraremovido.txt
filesystem.unlinkSync(newPath("seraremovido.txt"));

// Novo exemplo
filesystem.writeFileSync(
	newPath("seralido.txt"),
	"este arquivo será lido",
	utf8
);

// Este método lerá um arquivo e os dados serão guardados em uma variável para consulta posterior
const dadosArquivo = filesystem.readFileSync(newPath("seralido.txt"), utf8);
console.log(dadosArquivo);

// Novo exemplo
filesystem.writeFileSync(
	newPath("seraeditado.txt"),
	"O dado a seguir será substituído\n dado: Eu sou lindo",
	utf8
);
// Utilizando os métodos de leitura e escrita, podemos criar uma função que substituirá conteúdos do arquivo.
const substituir = (caminhoArquivo, dadosASubstituir, novosDados) => {
	// Dados do arquivo a ser modificado
	const arquivo = filesystem.readFileSync(caminhoArquivo, utf8);
	// modificaremos o dados do arquivo
	const novoArquivo = arquivo.replace(dadosASubstituir, novosDados);
	// Salvará o arquivo manipulado substuindo o arquivo inicial
	filesystem.writeFileSync(caminhoArquivo, novoArquivo, utf8);
};
// Arquivo final terá retorno: Paulo é lindo.
substituir(newPath("seraeditado.txt"), "Eu sou", "Paulo é");

// mude constante abaixo para 1 se deseja remover todos os arquivos criados durante a execução deste programa.
// Valor padrão deve ser 0 para que seja observado os arquivos criado no programa.
const cleanFiles = 0;
if (cleanFiles === 1) {
	filesystem.rmdirSync(path.join("./src/arquivos/"), { recursive: true });
}
